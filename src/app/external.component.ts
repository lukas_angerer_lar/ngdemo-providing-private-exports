import { Component, Injectable } from '@angular/core';

/**
 * This sneaky service is not exported, making it difficult to deal with
 */
@Injectable()
class SneakyService {
  public constructor(public name: string) {}
}

export { SneakyService as SneakyService$1 };

@Injectable()
export class NestedService {
  public constructor(sneaky: SneakyService) {
    console.log('NestedService got injected with', sneaky);
  }
}

@Component({
  selector: 'external',
  template: `<code>ExternalComponent: {{ sneaky.name }}</code>`,
  providers: [
    {
      provide: SneakyService,
      useFactory: () => new SneakyService('you can\'t touch this'),
    },
    NestedService,
  ]
})
export class ExternalComponent  {
  public constructor(
    public readonly sneaky: SneakyService,
    public readonly nested: NestedService
  ) {
  }
}

/**
 * Prod build =>
 * > ERROR in src\app\external.component.ts(6,1): Error during template compile of 'ExternalComponent'
 * > References to a non-exported class are not supported in decorators but SneakyService was referenced.
 * > Consider exporting 'SneakyService'.
 */
