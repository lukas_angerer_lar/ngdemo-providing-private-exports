import { GridComponent } from '@progress/kendo-angular-grid';
import { EditService$1 as EditService } from '@progress/kendo-angular-grid';
import { Component, Optional, Inject } from '@angular/core';

// index of the EditService in the list of constructor arguments for the GridComponent
const EDIT_SERVICE_INDEX = 7;
const EDIT_SERVICE_TYPE = GridComponent['ctorParameters'] ? GridComponent['ctorParameters']()[EDIT_SERVICE_INDEX].type : null;

console.log("TYPE", EDIT_SERVICE_TYPE);

@Component({
  selector: 'custom-kendo',
  template: `<code>CustomKendoComponent: {{ edit.constructor.name }}</code>`,
  providers: [
    {
      provide: EditService,
      useFactory: () => new EDIT_SERVICE_TYPE(null),
    },
  ]
})
export class CustomKendoComponent  {
  public constructor(@Inject(EditService) public readonly edit: any) {
    window['GridComponent'] = GridComponent;
    
    // Works in:
    // StackBlitz
    if ((Reflect as any).getMetadata) {
      console.log('GridComponent metadata (Reflect        )', (Reflect as any).getMetadata('design:paramtypes', GridComponent));
      console.log('GridComponent metadata (__annotations__)', GridComponent['__annotations__']);
    } else {
      // this is why we have to include the core-js shim "reflect-metadata" when we want to use that in our own code;
      // since Angular only uses this for the "compilation process", it gets tree shaken out when building AOT
      console.error('Reflect.getMetadata not available');
    }
    // Works in:
    // Angular 8 - ng serve
    if (GridComponent['ctorParameters']) {
      console.log('GridComponent metadata (ctorParameters )', GridComponent['ctorParameters']());
      console.log('GridComponent metadata (ctorParameters )', GridComponent['ctorParameters']()[EDIT_SERVICE_INDEX].type);
    } else {
      // it seems that the ctorParameters are only available for "things that are compiled as libraries"
      console.error('GridComponent.ctorParameters not available');
    }

    console.log('Service methods:', Object.keys(edit.constructor.prototype));
    console.log('Type equality:', EditService === edit.constructor);
  }
}
