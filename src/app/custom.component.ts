import { Component, Optional, Inject } from '@angular/core';
import { ExternalComponent, NestedService } from './external.component';
import { SneakyServiceProxy2 as SneakyService } from './sneaky-proxy';

@Component({
  selector: 'custom',
  template: `<code>CustomComponent: {{ sneaky ? sneaky.name : 'no name' }}</code>`,
  providers: [
    {
      provide: SneakyService,
      useFactory: () => new SneakyService('l33t h4xx0r'),
    },
    NestedService,
  ]
})
export class CustomComponent extends ExternalComponent {
  public constructor(@Inject(SneakyService) sneaky: any, nested: NestedService) {
    super(sneaky, nested);

    window['ExternalComponent'] = ExternalComponent;

    if ((Reflect as any).getMetadata) {
      console.log('ExternalComponent metadata (Reflect        )', (Reflect as any).getMetadata('design:paramtypes', ExternalComponent));
      console.log('ExternalComponent metadata (__annotations__)', ExternalComponent['__annotations__']);
    } else {
      console.error('Reflect.getMetadata not available');
    }
    if (ExternalComponent['ctorParameters']) {
      console.log('ExternalComponent metadata (ctorParameters )', ExternalComponent['ctorParameters']());
      console.log('ExternalComponent metadata (ctorParameters )', ExternalComponent['ctorParameters']()[0].type);
    } else {
      console.error('ExternalComponent.ctorParameters not available');
    }
  }
}
