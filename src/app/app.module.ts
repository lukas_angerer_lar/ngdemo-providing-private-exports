import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { ExternalComponent } from './external.component';
import { CustomComponent } from './custom.component';
import { CustomKendoComponent } from './custom-kendo.component';

@NgModule({
  imports:      [ BrowserModule, FormsModule ],
  declarations: [
    AppComponent,
    ExternalComponent,
    CustomComponent,
    CustomKendoComponent,
  ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
