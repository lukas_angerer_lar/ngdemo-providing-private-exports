import { Type } from '@angular/core';
import { ExternalComponent, SneakyService$1 } from './external.component';

// must be exported, otherwise there is a compiler error with prod build
// functions cannot be called in decorators since the Angular AOT compiler
// must be able to evaluate everything _statically_
export function findSneakyService(): Type<any> {
  const annotations = ExternalComponent['__annotations__'];

  if (annotations) {
    for (let annotation of annotations) {
      if (annotation.providers) {
        for (let provider of annotation.providers) {
          if (provider.provide && provider.provide.name === 'SneakyService') {
            console.log('resolved service', provider.provide);
            return provider.provide;
          }
        }
      }
    }
  }

  console.error('could not resolve SneakyService');
  return null;
}

export const SneakyServiceProxy1 = findSneakyService();

export { SneakyService$1 as SneakyServiceProxy2 };
